package com.test;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.*;
import java.io.*;

import static com.test.Message.*;

public class Consumer {

    private BlockingQueue<Message> queue;
    private Thread consumerThread = null;
    ArrayList<String> list = new ArrayList<String>();

    public Consumer(BlockingQueue<Message> queue) {
        this.queue = queue;
    }

    public void startConsuming() {
        consumerThread = new Thread(new Runnable() {
            @Override
            public void run() {
                while (true) {
                    try {
                        Message message = queue.take();
                        list.add(message.toString());
                        new PrintToFile(message);
                      //  System.out.println(message);
                    } catch (InterruptedException e) {
                        // executing thread has been interrupted, exit loop
                        break;
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                new Message.BufferMessage(list);
            }
        });
        consumerThread.start();
    }

    public void stopConsuming() {
        consumerThread.interrupt();
    }
}
