package com.test;

import java.io.*;


public class PrintToFile {
    PrintToFile(Message m) throws IOException {
        FileOutputStream file = new FileOutputStream("file.txt", true);
        byte[] buffer = m.toString().getBytes();
        file.write(buffer);
        file.close();
    }
}


