package com.test;

import java.text.SimpleDateFormat;
import java.util.*;

public class Message {

    private long timestamp;
    private Priority priority;
    private String text;
    public Message(long timestamp, Priority priority, String text) {
        this.timestamp = timestamp;
        this.priority = priority;
        this.text = text;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public Priority getPriority() {
        return priority;
    }

    public String getText() {
        return text;
    }

    @Override
    public String toString() {
        SimpleDateFormat sdf = new SimpleDateFormat("dd MMM yyyy HH:mm", Locale.ENGLISH);
        Date resultdate = new Date(timestamp);
        return priority + " " + sdf.format(resultdate) + " " + text + "\r\n";
    }

    public enum Priority {

        HIGH, MEDIUM, LOW;

    }

    static class BufferMessage {
        BufferMessage(ArrayList list) {
            int i = 0;
            int j = 10;
            while ((i <= list.size() - 10) && j <= (list.size())) {
                Collections.sort(list.subList(i, j)); //, new Message.Sorted()
                i += 10;
                j += 10;
            }
            for (Object s : list)
                System.out.println(s);
        }
    }

 /*   static class Sorted implements Comparator<Message> {
        @Override
        public int compare(Message o1, Message o2) {
            String s1 = String.valueOf(o1.toString());
            String s2 = String.valueOf(o2.toString());

            return s1.length()-s2.length();
        }
    }*/

}




